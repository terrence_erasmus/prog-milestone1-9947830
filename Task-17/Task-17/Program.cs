﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new Dictionary<string, string>();

            user.Add("Sarah", "22");
            user.Add("Mich", "34");
            user.Add("James", "41");

            foreach (var x in user)

                Console.WriteLine($"{x.Key} is {x.Value}");
        }
    }
}
