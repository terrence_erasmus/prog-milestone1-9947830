﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            var age = 0;

            string name;

            Console.WriteLine("What is your name?");
            name = Console.ReadLine();

            Console.WriteLine("What is your age?");
            age = int.Parse(Console.ReadLine());

            Console.WriteLine("My name is " + name + " and I am " + age + " years old.");
            Console.WriteLine("My name is {0} and I am {1} years old.", name, age);
            Console.WriteLine($"My name is {name}  and I am {age}  years old.");
        }
    }
}
