﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var day = 0;
            var month = 0;

            Console.WriteLine("What day were you born on?");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine("What month were you born in?");
            month = int.Parse(Console.ReadLine());

            Console.WriteLine($"You were born on day {day} in month {month} ");
        }
    }
}
