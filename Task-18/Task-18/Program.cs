﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new Dictionary<string, string>();

            user.Add("Sarah", "March 23");
            user.Add("Mich", "April 02");
            user.Add("James", "June 17");

            foreach (var x in user)

                Console.WriteLine($"{x.Key} {x.Value}");
        }
    }
}
