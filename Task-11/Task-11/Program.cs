﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a year");

            int year = Convert.ToInt32(Console.ReadLine());

            if (DateTime.IsLeapYear(year))
            {
                Console.WriteLine("Yes this is a Leap Year");
            }

            else
                Console.WriteLine("No this is not a Leap Year");

        }
    }
}
